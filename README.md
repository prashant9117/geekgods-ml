Machine Learning - IPL data
---------------------------
We have used python and assisting libraries to predict the IPL winner based on the data provided. 
Feature sampled/engineered the data and used preprocessing and different algorithm to create models and make the prediction.


Prerequisites
-------------
Python 3.6
Python libraries like numpy, pandas, matplotlib
ML library scikit-learn
Jupyter


System Setup
------------
Setup a Virtual env
Install Virtual Environment

pip install virtualenv
Create Virtual Environment

virtualenv <directory-name> -p python3.6
Activate Virtual Environment

source <directory-name ref>/bin/activate


Data
----
The root folder contains matches.csv and deliveries.csv.



Start Project
run jupyter notebook from inside your project root.


Authors
Yogesh Sidhwani
Prashant Tiwari
Priyanka Verma


License
This project is licensed under the MIT License - see the LICENSE.md file for details
